﻿create database ResistenciaDB;
use ResistenciaDB;


create table tb_resistencia
(
	id_resistencia		int	primary key auto_increment,
    nm_pessoa			varchar(100),
    ds_mensagem			varchar(200)
);

select * from tb_resistencia;

insert into tb_resistencia (nm_pessoa, ds_mensagem)
	 values ('Bruno', 'Hard Work Beats Talent When Talent doesnt Hard Work');